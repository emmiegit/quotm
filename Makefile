# Makefile
#
# quotm - Manage your quotations library.
# Copyright (c) 2015 Ammon Smith
# 
# quotm is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
# 
# quotm is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quotm.  If not, see <http://www.gnu.org/licenses/>.
#
INSTALL_LOCATION=/usr/local/bin

.PHONY: all build debug man install uninstall clean

all: build man

build:
	make -C src quotm

debug:
	make -C src EXTRA_FLAGS=-g clean quotm

man:
	make -C man

install: build
	install quotm $(INSTALL_LOCATION)
	make -C man install

uninstall:
	rm $(INSTALL_LOCATION)/quotm
	make -C man uninstall

clean:
	make -C man clean
	make -C src clean

