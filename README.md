# quotations-manager
A CLI program to manage all your saved quotations from your favorite thinkers.

This project is currently unstable as it is in the middle of being moved from Python to C. If you wish to use this program regardless, download the python file and use that. It will manage quotes in an interactive manner and store them in a `.pickle` file, which is not compatible with the future C-based version of this program.
