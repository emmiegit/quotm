#!/usr/bin/env python2
from __future__ import print_function, with_statement

try:
    import cPickle as pickle
except ImportError:
    import pickle

import os, sys, random

try:
    input = raw_input
except NameError:
    pass

try:
    TEXT_EDITOR = os.environ['VISUAL']
except KeyError:
    try:
        TEXT_EDITOR = os.environ['EDITOR']
    except KeyError:
        TEXT_EDITOR = 'vi'

PICKLE_PROTOCOL = 2
TEXT_VIEWER = "less"
TEMP_DIR = "/tmp"
QUOTE_FORMAT = "%s\n- %s"
LICENSE = """\
quotm - Manage your quotations library.
Copyright (c) 2015 Ammon Smith

quotm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

quotm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with quotm.  If not, see <http://www.gnu.org/licenses/>."""

def confirmation(prompt):
    response = input(prompt)
    return response.lower() in ("y", "yes")

def print_quote(quote):
    print((QUOTE_FORMAT % quote).rstrip())

class QuotationLibrary:
    def __init__(self, filename=None):
        if filename == None:
            quotations = set()
        else:
            quotations = pickle.load(filename)

        # quotations = { ( quotation, author )... }
        if type(quotations) != set:
            raise TypeError("Quotations collection object must be a set.")

        # Sanity checks
        for quotation in quotations:
            if type(quotation) != tuple:
                raise TypeError("Quotation storage object found that is not a tuple.")
            if len(quotation) != 2:
                raise ValueError("Quotation storage object found of length %d." % len(quotation))
            if not (type(quotation[0]) == type(quotation[1]) == str):
                raise TypeError("Quotation text and author must be strings, not %s and %s." % (type(quotation[0]), type(quotation[1])))

        self.quotes = quotations

    def dump(self, filename):
        pickle.dump(self.quotes, filename, PICKLE_PROTOCOL)

    def count(self):
        return len(self.quotes)

    def add_quote(self, quote, author):
        if type(quote) != str:
            raise TypeError("Quote text must be a string, not %s." % type(quote))
        if type(author) != str:
            raise TypeError("Quote author must be a string, not %s." % type(author))

        self.quotes.add((quote, author))

    def remove_quote(self, old_quote, old_author):
        try:
            self.quotes.remove((old_quote, old_author))
        except KeyError:
            raise KeyError("Specified quotation not found.")

    def edit_quote(self, old_quote, old_author, new_quote, new_author):
        self.remove_quote(old_quote, old_author)
        self.add_quote(new_quote, new_author)

    def all_quotes(self):
        allquotes = ""

        for quotation in self.quotes:
            allquotes += QUOTE_FORMAT % quote

        return allquotes

    def export_to_file(self, fh):
        fh.write(self.all_quotes())

    def random(self):
        return random.choice(list(self.quotes))

    def search(self, term):
        matches = set()

        for quotation in self.quotes:
            if term in quotation[0] or term in quotation[1]:
                matches.add(quotation)

        return matches

    def interactively_add_quote(self, add_help_text=True):
        fn = "%s/quot-new-%d.txt" % (TEMP_DIR, random.randint(1, 1000))

        try:
            with open(fn, 'w+') as fh:
                if add_help_text:
                    fh.write("Add your quote here.\n")
                    fh.write("If the last line begins with \"- \", then it will be interpreted as the author.\n")
                    fh.write("To cancel this action, make this file empty and quit the editor.")
        except StandardError as err:
            print("Error: %s" % err)
            return

        os.system("%s %s" % (TEXT_EDITOR, fn))

        try:
            with open(fn, 'r') as fh:
                lines = fh.readlines()
            os.remove(fn)
        except StandardError as err:
            print("Error: %s" % err)
            return

        return self.add_from_lines(lines, None, None)

    def interactively_edit_quote(self, quote, author):
        if (quote, author) not in self.quotes:
            print("Warning: quote not found to edit.")
        else:
            self.remove_quote(quote, author)

        fn = "%s/quot-edit-%d.txt" % (TEMP_DIR, random.randint(1, 1000))

        try:
            with open(fn, 'w+') as fh:
                fh.write(QUOTE_FORMAT % (quote, author))
        except StandardError as err:
            print("Error: %s" % err)
            return quote, author

        os.system("%s %s" % (TEXT_EDITOR, fn))

        try:
            with open(fn, 'r') as fh:
                lines = fh.readlines()
            os.remove(fn)
        except StandardError as err:
            print("Error: %s" % err)
            return quote, author

        return self.add_from_lines(lines, quote, author)

    def add_from_lines(self, lines, quote, author):
        while len(lines) and not lines[-1].strip():
            lines = lines[:-1]

        if not len(lines):
            print("File was empty: no changes have been made.")
            return quote, author

        if lines[-1].startswith("- "):
            if not lines[-2].strip():
                del lines[-2]

            quote = '\n'.join(lines[:-1])
            author = lines[-1][2:]
        else:
            quote = '\n'.join(lines)
            author = "Unknown"

        self.add_quote(quote, author)
        return quote, author

    def list_quotes(self, quotes):
        quit_loop = False
        i = 0
        quotes = list(quotes)
        while i < len(quotes):
            quote = quotes[i]

            print_quote(quote)
            while True:
                try:
                    cmd = input(":").strip().lower()
                except KeyboardInterrupt:
                    quit_loop = True
                    break

                if cmd in ("help", "?"):
                    print("Commands:")
                    print("a - Add quote")
                    print("b - Go back one quote")
                    print("d - Delete quote")
                    print("e - Edit quote")
                    print("p - Print quote")
                    print("q - Quit listing quotes")
                    print("? - Print help")
                    print("<newline> - Next quote")
                elif not cmd:
                    i += 1
                    break
                elif cmd == "a":
                    try:
                        self.interactively_add_quote()
                        print("Quote added.")
                    except StandardError as err:
                        print("Error: %s" % (err,))
                elif cmd == "b":
                    i -= 1
                    if i < 0:
                        print("Hit the first item.")
                        i = 0
                    else:
                        print_quote(quotes[i])
                elif cmd == "d":
                    try:
                        self.delete_quote(*quote)
                        print("Quote deleted.")
                    except StandardError as err:
                        print("Error: %s" % (err,))
                elif cmd == "e":
                    try:
                        quote = self.interactively_edit_quote(*quote)
                        print("Quote edited.")
                    except StandardError as err:
                        print("Error: %s" % (err,))
                elif cmd == "p":
                    print_quote(quote)
                elif cmd == "q":
                    i = len(quotes)
                    break
                else:
                    print("Unknown command: \"%s\"." % cmd)
                    print("Enter '?' for help.")


def save(library):
    try:
        with open(data_file, 'w+') as fh:
            library.dump(fh)
    except StandardError as err:
        print("\nUnable to save quote library at \"%s\": %s" % (data_file, err))

if __name__ == "__main__":
    print("Quotation Manager")
    print("For help type 'help'.")

    data_file = os.path.dirname(sys.argv[0]) + os.sep + "quotations.pickle"
    if data_file.startswith(os.sep):
        data_file = "quotations.pickle"

    if os.path.exists(data_file):
        try:
            with open(data_file, 'r') as fh:
                library = QuotationLibrary(fh)
        except StandardError as err:
            print("Error while opening quotation library: %s" % err)
            print("Proceeding with empty library.")
            library = QuotationLibrary()
    else:
        print("Cannot find data file at \"%s\". Creating blank quote library." % data_file)
        library = QuotationLibrary()


    if not os.path.exists(TEMP_DIR):
        os.mkdir(TEMP_DIR)

    while True:
        try:
            cmd = input("> ").strip().lower()
        except KeyboardInterrupt:
            print()
            continue
        except EOFError:
            break

        if cmd == "help":
            print("Available commands:")
            print(" addhelp  Add a new quotation with help text (for new users)")
            print(" add      Add a new quotation")
            print(" count    List the number of loaded quotes")
            print(" dump     Create a text file with all quotes")
            print(" help     Print this help text")
            print(" license  Print this software's license")
            print(" list     Interactively list all quotations")
            print(" random   Prints a random quote")
            print(" save     Save all quotations to disk now")
            print(" search   Searches for a quotation based on a search term")
            print(" quit     Exits this application")
        elif not cmd:
            pass
        elif cmd == "addhelp":
            try:
                library.interactively_add_quote()
            except StandardError as err:
                print("Error: %s" % (err,))
        elif cmd == "add":
            try:
                library.interactively_add_quote(False)
            except StandardError as err:
                print("Error: %s" % (err,))
        elif cmd in ("count", "cnt"):
            print("%d quotations currently in library." % library.count())
        elif cmd in ("dump", "dmp"):
            while True:
                fn = input("Where would you like this file? ")
                if os.path.exists(fn) and not os.path.isdir(fn):
                    if not confirmation("This location already exists. Would you like to overwrite it? [y/N] "):
                        continue
                try:
                    with open(fn, 'w+') as fh:
                        library.export_to_file(fh)
                except:
                    print("Unable to write to %s." % fn)
                break
        elif cmd == "license":
            print(LICENSE)
        elif cmd in ("list", "ls"):
            if library.count() == 0:
                print("No items in library.")
            else:
                library.list_quotes(library.quotes)
        elif cmd in ("random", "rand"):
            print_quote(library.random())
        elif cmd == "save":
            save(library)
        elif cmd == "search":
            try:
                term = input("Enter a search term: ")
            except KeyboardInterrupt:
                continue

            results = library.search(term)
            if len(results) == 0:
                print("No matches found.")
            else:
                library.list_quotes(results)

        elif cmd in ("exit", "quit", "stop", "term"):
            break
        else:
            print("Unknown command \"%s\"." % cmd)

    save(library)

