# Makefile
#
# quotm - Manage your quotations library.
# Copyright (c) 2015 Ammon Smith
# 
# quotm is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
# 
# quotm is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quotm.  If not, see <http://www.gnu.org/licenses/>.

.PHONY: install uninstall clean

INSTALL_LOCATION=/usr/share/man
GZ_FILES=quotm.1.gz \
		 quotm-add.1.gz \
		 quotm-cat.1.gz \
		 quotm-dump.1.gz \
		 quotm-edit.1.gz \
		 quotm-find.1.gz \
		 quotm-help.1.gz \
		 quotm-ls.1.gz \
		 quotm-rand.1.gz \
		 quotm-rm.1.gz \
		 quotm-tag.1.gz
PS_FILES=quotm.1.ps \
		 quotm-add.1.ps \
		 quotm-cat.1.ps \
		 quotm-dump.1.ps \
		 quotm-edit.1.ps \
		 quotm-find.1.ps \
		 quotm-help.1.ps \
		 quotm-ls.1.ps \
		 quotm-rand.1.ps \
		 quotm-rm.1.ps \
		 quotm-tag.1.ps

all: $(GZ_FILES)

pdf: $(PS_FILES)
	gs -q -sPAPERSIZE=letter -dNOPAUSE -dBATCH -sDEVICE=pdfwrite \
		-sOutputFile=quotm.pdf $(PS_FILES)

%.1.ps: %.1
	groff -Tps -mandoc $< > $<.ps

%.1.gz: %.1
	@echo "[GZ] $<"
	@cp -f $< _gz
	@gzip _gz
	@mv -f _gz.gz $<.gz

install:
	for section in 1; do \
		mkdir -p "$(INSTALL_LOCATION)/man$${section}/"; \
		cp *.$${section}.gz "$(INSTALL_LOCATION)/man$${section}/"; \
	done

uninstall:
	for section in 1; do \
		for fn in *.$${section}.gz; do \
			rm -f "$(INSTALL_LOCATION)/man$${section}/$${fn}" \
		done \
	done

clean:
	rm -f *.gz *.ps *.pdf *~ *#

