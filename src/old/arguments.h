/*
 * arguments.h
 *
 * quotm - Manage your quotations library.
 * Copyright (c) 2015 Ammon Smith
 * 
 * quotm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * quotm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with quotm.  If not, see <http://www.gnu.org/licenses/>.
 */

#define OPERATION_ADD    0
#define OPERATION_CAT    1
#define OPERATION_DUMP   2
#define OPERATION_EDIT   3
#define OPERATION_FIND   4
#define OPERATION_LIST   5
#define OPERATION_RANDOM 6
#define OPERATION_REMOVE 7
#define OPERATION_TAG    8

typedef struct {
    const char* file;
    const char* editor;
    char operation;
    bool interactive;
} options;

options* parseargs(int argc, const char* argv[]);

