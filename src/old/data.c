/*
 * data.c
 *
 * quotm - Manage your quotations library.
 * Copyright (c) 2015 Ammon Smith
 * 
 * quotm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * quotm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with quotm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "data.h"
#include "quotm.h"

static bool quote_matches(const qquery* query, const qentry* quote);
static long djb2(const char* str);

bool add_quote(qset* set, qentry* quote)
{
    if (set->capacity == set->size) {
        return false;
    }

    size_t i;
    for (i = quote->hash % set->capacity;
         set->array[i];
         i = (i + 1) % set->capacity);

    set->array[i] = quote;
    set->size++;
    return true;
}

bool remove_quote(qset* set, qentry* quote)
{
    size_t i;
    for (i = quote->hash % set->capacity;
         set->array[i];
         i = (i + 1) % set->capacity) {
        if (set->array[i] == quote) {
            set->array[i] = NULL;
            set->size--;
            return true;
        }
    }

    return false;
}

static bool quote_matches(const qquery* query, const qentry* quote)
{
    return (!query->hash_is_set || (query->hash == quote->hash)) &&
           (!query->author || !strcmp(query->author, quote->author)) &&
           (!query->body || !strcmp(query->body, quote->body));
}

qentry* find_quote(qset* set, const qquery* query)
{
    if (query->hash_is_set) {
        size_t i;
        for (i = query->hash % set->capacity;
             set->array[i];
             i = (i + 1) % set->capacity) {
            if (quote_matches(query, set->array[i])) {
                return set->array[i];
            }
        }

        return NULL;
    } else {
        size_t i;
        for (i = 0; i < set->capacity; i++) {
            if (quote_matches(query, set->array[i])) {
                return set->array[i];
            }
        }

        return NULL;
    }
}

void expand_set(qset* set, const size_t new_capacity)
{
    assert(new_capacity > set->capacity);

    qentry* old_array[] = set->array;
    qentry* new_array[] = realloc(set->array, (new_capacity * sizeof(qentry)));
    if (!new_array) {
        destroy_set(set);
        fprintf(stderr, "Error reallocating array.\n");
        cleanup(1);
    }

    set->array = new_array;

    size_t i, changed;
    for (i = 0, changed = 0; i < set->capacity && changed < set->size; i++) {
        if (old_array[i]) {
            add_quote(set, old_array[i]);
            changed++;
        }
    }

    set->capacity = new_capacity;
}


qset* new_set(const size_t capacity)
{
    qset* set = malloc(sizeof(qset));
    set->capacity = capacity;
    set->size = 0;
    set->array = calloc(capacity, sizeof(qentry));
    return set;
}

void destroy_set(qset* set)
{
    free(set->array);
    free(set);
}

static long djb2(const char* str)
{
    long hash = 5381;
    int c;

    while ((c = *str++)) {
        /* hash * 33 + c */
        hash = ((hash << 5) + hash) + c;
    }

    return hash;
}

void update_hash(qentry* quote)
{
    quote->hash = djb2(quote->author) ^ djb2(quote->body);
}

