/*
 * help.c
 *
 * quotm - Manage your quotations library.
 * Copyright (c) 2015 Ammon Smith
 * 
 * quotm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * quotm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with quotm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#include "help.h"

static void print_help_add();
static void print_help_cat();
static void print_help_dump();
static void print_help_edit();
static void print_help_find();
static void print_help_ls();
static void print_help_help();
static void print_help_rand();
static void print_help_rm();
static void print_help_tag();

extern const char* program_name;

void print_version()
{
    printf("Version %s.\n", VERSION_STRING);
    printf("Copyright (C) 2015 Ammon Smith.\n");
    printf("Licensed under the GNU General Public License v2.\n");
    printf("\n");
    printf("quotm is distributed in the hope that it will be useful,\n");
    printf("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
    printf("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
    printf("GNU General Public License for more details.\n");
}

void print_usage()
{
    printf("Usage: %s command [options] library-file\n", program_name);
    printf("Try running \"%s help\" for guidance.\n", program_name);
}

void print_help()
{
    printf("General usage: %s command [options] library-file\n", program_name);
    printf("Available commands are:\n");
    printf(" add     - Add a new quotation to the library.\n");
    printf(" cat     - Combine multiple quotation libraries.\n");
    printf(" dump    - Export a library to a human-readable file.\n");
    printf(" edit    - Edit a quotation already present in the library.\n");
    printf(" find    - Search for a quotation in the library.\n");
    printf(" ls      - List all quotations in the library.\n");
    printf(" help    - Get help on how to use this program.\n");
    printf(" rand    - Output a random quotation in the library.\n");
    printf(" rm      - Remove a quotation from the library.\n");
    printf(" tag     - ???\n");
    printf(" version - Print the version information and quit.\n");
    printf("\nUse \"%s help [command]\" to get usage for any command.\n", program_name);
}

void print_help_item(const char* item)
{
    if (!strcmp(item, "add")) {
        print_help_add();
    } else if (!strcmp(item, "cat")) {
        print_help_cat();
    } else if (!strcmp(item, "dump")) {
        print_help_dump();
    } else if (!strcmp(item, "edit")) {
        print_help_edit();
    } else if (!strcmp(item, "find")) {
        print_help_find();
    } else if (!strcmp(item, "ls")) {
        print_help_ls();
    } else if (!strcmp(item, "help")) {
        print_help_help();
    } else if (!strcmp(item, "rand")) {
        print_help_rand();
    } else if (!strcmp(item, "rm")) {
        print_help_rm();
    } else if (!strcmp(item, "tag")) {
        print_help_tag();
    } else {
        printf("No help information available for \"%s\".\n", item);
    }
}

static void print_help_add()
{
    printf("Usage: %s add [options] library-file\n", program_name);
}

static void print_help_cat()
{
    printf("Usage: %s cat [options] library-file... output-library-file\n", program_name);
    printf("Joins multiple quotation libraries together into a single library.\n");
    printf("This does not modify any of the source libraries.\n");
    printf("Available options:\n");
    printf(" -f, --force  If the output library file exists, overwrite it.\n");
}

static void print_help_dump()
{
    printf("Usage: %s dump format [options] library-file\n", program_name);
    printf("Outputs the specified quotation library in one of multiple\n");
    printf("human-readable formats.\n\n");
    printf("Available formats:\n");
    printf(" html - Create a simple HTML document.\n");
    printf(" md   - Create a Markdown document with all the quotes.\n");
    printf(" text - Create a plain text document.\n");
    printf("\n");
    printf("Available options:\n");
    printf("(none)\n");
}

static void print_help_edit()
{
    printf("Usage: %s edit [options] library-file\n", program_name);
    printf("Available options:\n");
    printf("   --editor=[PROGRAM]  Use PROGRAM instead of your configured $VISUAL or $EDITOR.\n");
}

static void print_help_find()
{
    printf("Usage: %s find search-text library-file\n", program_name);
    printf("Searches within the specified library for the specified\n");
    printf("search text.\n\n");
    printf("Available options:\n");
    printf(" -A, --author  Only search in the 'author' field.\n");
    printf(" -B, --body    Only search in the text of the quotation's body.\n");
    printf(" -R, --regex   Interpret 'search-text' as a regular expression\n");
    printf("               and search the library for matches.\n");
    printf(" -q, --quiet   Do not output any matches, just exit with a non-zero\n");
    printf("               exit code if nothing is found.\n");
}

static void print_help_help()
{
    printf("Usage: %s help [options] item-or-command\n", program_name);
    printf("Prints documentation related to this program.\n\n");
    printf("You may specify one or more commands or other help items to get\n");
    printf("specific help for them. Or, you may invoke \"help\" without any\n");
    printf("arguments to get a summary of all help items.\n\n");
    printf("Available options:\n");
    printf(" -i, --interactive   Run in interactive mode.\n");
}

static void print_help_ls()
{
    printf("Usage: %s ls [options] library-file\n", program_name);
    printf("List all the quotations found in the library.\n\n");
    printf("Available options:\n");
    printf(" -c, --count   Only print the number of quotes in the library.\n");
}

static void print_help_rand()
{
    printf("Usage: %s rand [options] library-file\n", program_name);
    printf("Selects a random quotation from the library and\n");
    printf("prints it to stdout.\n\n");
    printf("Available options:\n");
    printf(" -f, --format=[FORMAT]  Output the random quotation in the given FORMAT.\n");
    printf("\nFORMAT accepts the following sequences:\n");
    printf(" %%%%   A literal %%.\n");
    printf(" %%a    The author of the quotation.\n");
    printf(" %%b    The text body of the quotation.\n");
}

static void print_help_rm()
{
    printf("Usage: %s rm [options] library-file\n", program_name);
}

static void print_help_tag()
{
    printf("Usage: %s tag [options]library-file\n", program_name);
}

