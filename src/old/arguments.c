/*
 * arguments.c
 *
 * quotm - Manage your quotations library.
 * Copyright (c) 2015 Ammon Smith
 * 
 * quotm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * quotm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with quotm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arguments.h"
#include "help.h"
#include "quotm.h"

static void parseflags(const char* argument, options* options);
static void parseshortflag(const char flag, options* options);
static void parselongflag(const char* flag, options* options);

options* parseargs(int argc, const char* argv[])
{
    options* options = malloc(sizeof(options));

    /* If the user entered no arguments */
    if (argc == 1) {
        print_usage();
        cleanup(1);
    }

    /* If the user entered only one argument */
    if (!strcmp(argv[1], "help") ||
        !strcmp(argv[1], "-help") ||
        !strcmp(argv[1], "--help")) {

        /* Check if the user is asking for help on a specific item */
        if (argc > 2) {
            int i;
            for (i = 2; i < argc; i++) {
                print_help_item(argv[i]);
            }
        } else {
            print_help();
        }
        
        cleanup(0);
    } else if (!strcmp(argv[1], "version")) {
        print_version();
        cleanup(0);
    } else if (argc == 2) {
        print_usage();
        cleanup(1);
    }

    /* Determine the operation the user wants to perform */
    if (!strcmp(argv[1], "add")) {
        options->operation = OPERATION_ADD;
    } else if (!strcmp(argv[1], "cat")) {
        options->operation = OPERATION_CAT;
    } else if (!strcmp(argv[1], "dump")) {
        options->operation = OPERATION_DUMP;
    } else if (!strcmp(argv[1], "edit")) {
        options->operation = OPERATION_EDIT;
    } else if (!strcmp(argv[1], "find")) {
        options->operation = OPERATION_FIND;
    } else if (!strcmp(argv[1], "list")) {
        options->operation = OPERATION_LIST;
    } else if (!strcmp(argv[1], "rand")) {
        options->operation = OPERATION_RANDOM;
    } else if (!strcmp(argv[1], "rm")) {
        options->operation = OPERATION_REMOVE;
    } else if (!strcmp(argv[1], "tag")) {
        options->operation = OPERATION_TAG;
    } else {
        fprintf(stderr, "Unknown command: \"%s\". Try running \"quotm help\" for guidance.\n", argv[1]);
        cleanup(1);
    }

    /* Determine the library file from the last argument */
    options->file = argv[argc - 1];

    /* Read all flags passed by the user */
    int i;
    for (i = 2; i < argc - 1; i++) {
        /* Obey the GNU style -- argument to stop argument passage */
        if (!strcmp(argv[i], "--")) {
            break;
        }

        parseflags(argv[i], options);
    }

    return options;
}

static void parseflags(const char* argument, options* options)
{
    const int len = strlen(argument);

    /* Make sure the argument is long enough to process */
    if (len < 2) {
        fprintf(stderr, "Unknown option: \"%s\". Try running \"quotm help\" for guidance.\n", argument);
        cleanup(1);
    }

    if (argument[0] == '-') {
        /* Determine if the option is short (-) or long (--) */
        if (argument[1] == '-') {
            parselongflag((char*)(argument + 2), options);
        } else {
            int i;
            for (i = 1; i < len; i++) {
                parseshortflag(argument[i], options);
            }
        }
    } else {
        /* Assume that this is an implicit list of short flags, like rm -rf */
        int i;
        for (i = 0; i < len; i++) {
            parseshortflag(argument[i], options);
        }
    }
}

static void parseshortflag(const char flag, options* options)
{
    switch(flag) {
        case 'i':
            options->interactive = true;
            break;
        default:
            fprintf(stderr, "Unknown option: \"-%c\". Try running \"quotm help\" for guidance.\n", flag);
            cleanup(1);
    }
}

static void parselongflag(const char* flag, options* options)
{
    if (!strcmp(flag, "interactive")) {
        options->interactive = true;
    } else {
        fprintf(stderr, "Unknown option: \"--%s\". Try running \"quotm help\" for guidance.\n", flag);
        cleanup(1);
    }
}

