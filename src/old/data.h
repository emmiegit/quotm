/*
 * data.h
 *
 * quotm - Manage your quotations library.
 * Copyright (c) 2015 Ammon Smith
 * 
 * quotm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * quotm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with quotm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>

typedef struct {
    long hash;
    char* author;
    char* body;
} qentry;

typedef struct {
    size_t size;
    size_t capacity;
    qentry* array[];
} qset;

typedef struct {
    const long hash;
    const bool hash_is_set;
    const char* author;
    const char* body;
} qquery;

bool add_quote(qset* set, qentry* quote);
bool remove_quote(qset* set, qentry* quote);
qentry* find_quote(qset* set, const qquery* query);
void destroy_set(qset* set);
int hash_quote(const char* author, const char* body);

