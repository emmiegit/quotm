/*
 * SignalHandler.cpp
 *
 * quotm - Manage your quotations library.
 * Copyright (c) 2015 Ammon Smith
 * 
 * quotm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * quotm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with quotm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <csignal>
#include <iostream>
#include "Main.h"
#include "SignalHandler.h"

static void handle_signal(const int signum);

void set_up_handlers()
{
    signal(SIGTERM, handle_signal);
    signal(SIGINT, handle_signal);
    signal(SIGHUP, handle_signal);
}

static void handle_signal(const int signum)
{
    switch (signum) {
        case SIGTERM:
            std::cerr << "Termination signal received. Exiting..." << std::endl;
            cleanup(0);
            break;
        case SIGINT:
            std::cerr << "Interrupt signal received. Exiting..." << std::endl;
            cleanup(0);
            break;
        case SIGHUP:
            std::cerr << "Hangup signal received. Exiting..." << std::endl;
            cleanup(signum);
            break;
    }
}

