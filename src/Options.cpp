/*
 * Option.cpp
 *
 * quotm - Manage your quotations library.
 * Copyright (c) 2015 Ammon Smith
 * 
 * quotm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * quotm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with quotm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include "Options.h"

using namespace std;

static string get_editor();
static string get_pager();

Options::Options()
{
    editor = get_editor();
    pager = get_pager();
}

static string get_editor()
{
    string editor = getenv("VISUAL");

    if (!editor) {
        editor = getenv("EDITOR");

        if (!editor) {
            editor = "vi";
        }
    }

    return editor;
}

static string get_pager()
{
    string pager = getenv("PAGER");

    if (!pager) {
        pager = "less";
    }

    return pager;
}

